job "network-ddns" {
  name = "Network DDNS (Cloudflare)"
  type = "service"
  region = "se"
  datacenters = ["soc"]
  namespace = "c3-networking"

  priority = 60
  
  constraint {
    attribute = "${meta.c3.role}"
    value = "node"
  }

  group "carboncollins.se" {
    count = 1

    service {
      provider = "consul"
      name     = "dynamic-dns"
      task     = "soc"

      tags = [
        "cloudflair",
        "ddns",
        "soc.carboncollins.se"
      ]
    }

    task "soc" {
      driver = "docker"

      vault {
        policies = ["service-cloudflair-carboncollins-se"]
      }

      config {
        image = "favonia/cloudflare-ddns:latest"
      }

      resources {
        cpu = 100
        memory = 10
      }

      env {
        TZ   = "[[ .defaultTimezone ]]"
        PUID = "[[ .defaultUserId ]]"
        PGID = "[[ .defaultGroupId ]]"

        PROXIED      = "false"
        // IP4_PROVIDER = "cloudflare.trace"
        IP6_PROVIDER = "none"

        UPDATE_CRON     = "@every 5m"
        UPDATE_ON_START = "true"
        UPDATE_TIMEOUT  = "30s"
      }

      template {
        data = <<EOH
          {{ with secret "c3kv/data/api/cloudflair/carboncollins.se" }}
          CF_API_TOKEN={{ index .Data.data "token" }}
          IP4_DOMAINS=soc.{{ index .Data.data "zone" }}
          {{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env = true
      }
    }
  }

  group "carboncollins.uk" {
    count = 1

    service {
      provider = "consul"
      name     = "dynamic-dns"
      task     = "soc"

      tags = [
        "cloudflair",
        "ddns",
        "soc.carboncollins.uk"
      ]
    }

    task "soc" {
      driver = "docker"

      vault {
        policies = ["service-cloudflair-carboncollins-uk"]
      }

      config {
        image = "favonia/cloudflare-ddns:latest"
      }

      resources {
        cpu = 100
        memory = 10
      }

      env {
        TZ   = "[[ .defaultTimezone ]]"
        PUID = "[[ .defaultUserId ]]"
        PGID = "[[ .defaultGroupId ]]"

        PROXIED      = "false"
        // IP4_PROVIDER = "cloudflare.trace"
        IP6_PROVIDER = "none"

        UPDATE_CRON     = "@every 5m"
        UPDATE_ON_START = "true"
        UPDATE_TIMEOUT  = "30s"
      }

      template {
        data = <<EOH
          {{ with secret "c3kv/data/api/cloudflair/carboncollins.uk" }}
          CF_API_TOKEN={{ index .Data.data "token" }}
          DOMAINS=soc.{{ index .Data.data "zone" }}
          {{ end }}
        EOH

        destination = "secrets/cloudflare.env"
        env = true
      }
    }
  }

  reschedule {
    delay = "10s"
    delay_function = "exponential"
    max_delay = "10m"
    unlimited = true
  }

  meta {
    gitSha = "[[ .gitSha ]]"
    gitBranch = "[[ .gitBranch ]]"
    pipelineId = "[[ .pipelineId ]]"
    pipelineUrl = "[[ .pipelineUrl ]]"
    projectId = "[[ .projectId ]]"
    projectUrl = "[[ .projectUrl ]]"
  }
}
