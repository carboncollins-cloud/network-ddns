# Network - DDNS

[[_TOC_]]

## Description

Dynamic DNS for updating the hq subdomain on both `carboncollins.se` and `carboncollins.uk` on [Cloudflare - DNS](https://www.cloudflare.com/en-gb/dns/)
