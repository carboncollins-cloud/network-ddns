# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

## [2022-05-08]

### Added
- Constraint to service nodes

### Changed
- Increased job priority

## [2022-04-23]

### Added
- custom name for Nomad job

### Changed
- Updated all links to point to new repository location
- Moved repository to [CarbonCollins - Cloud](https://gitlab.com/carboncollins-cloud) GitLab Group
- Pipeline template to new cloud variant
- Use new vault secret structure
